import { useEffect, useState } from "react";

function App() {
  const [users, setUsers] = useState([])
  const [tasks, setTasks] = useState([])
  const [taskDetails, setTaskDetails] = useState({})

  useEffect(() => {
    fetch('http://localhost:3000/users').then(res => res.json()).then(setUsers).catch(console.error)
  }, [])

  const handleUserTasks = (id) => {
    fetch(`http://localhost:3001/lists/users/${id}`).then(res => res.json()).then(setTasks).catch(console.error)
  }

  const handleTaskDetails = (id) => {
    fetch(`http://localhost:3001/lists/${id}`).then(res => res.json()).then(setTaskDetails).catch(console.error)
  }

  return (
    <>
      <div>
        <h2>Lista de usuarios `/users`</h2>
        <ul>
          {users.map(user => (
            <div key={user.id}>
              <li >
                {user.name}
              </li>
              <button onClick={() => handleUserTasks(user.id)}>Tareas</button>
            </div>
          ))}
        </ul>
      </div>
      <div>
        <h2>Lista de tareas por usuario `/lists/users/userId`</h2>
        <ul>
          {tasks.map(({ id, task }) => (
            <div key={id}>

              <li>
                {task}
              </li>
              <button onClick={() => handleTaskDetails(id)}>Detalles</button>
            </div>
          ))}
        </ul>
      </div>
      <div>
        <h2>Detalles de una tarea `/lists/taskId`</h2>
        <ul>
          <li>
            {JSON.stringify(taskDetails)}
          </li>
        </ul>
      </div>
    </>
  );
}

export default App;
