package database

import "todo_service/model"

var Items = []model.Item{
	{Id: 1, UserId: 1, Task: "Estudiar"},
	{Id: 2, UserId: 1, Task: "Correr"},
	{Id: 3, UserId: 2, Task: "Cocinar"},
	{Id: 4, UserId: 2, Task: "Limpiar"},
	{Id: 5, UserId: 2, Task: "Pasear mascota"},
	{Id: 6, UserId: 3, Task: "Leer"},
}
