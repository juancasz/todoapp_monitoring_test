package controllers

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
	"strconv"
	"todo_service/database"
	"todo_service/model"

	"github.com/go-chi/chi/v5"
)

type ToDoController interface {
	CheckService(http.ResponseWriter, *http.Request)
	GetUsersTasks(http.ResponseWriter, *http.Request)
	GetTaskInfo(http.ResponseWriter, *http.Request)
}

func NewToDoController() ToDoController {
	return &toDoController{}
}

type toDoController struct {
}

func (t *toDoController) CheckService(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)
	w.Write([]byte("Todo service is up"))
}

func (t *toDoController) GetUsersTasks(w http.ResponseWriter, r *http.Request) {
	userId, err := strconv.Atoi(chi.URLParam(r, "user_id"))
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(fmt.Sprintf("Failed: %s", err.Error())))
		return
	}

	log.Println("url:", fmt.Sprintf("http://localhost:%s/users/%s", os.Getenv("USERS_SERVICE_PORT"), chi.URLParam(r, "user_id")))

	responseUser, err := http.Get(fmt.Sprintf("http://users-api:%s/users/%s", os.Getenv("USERS_SERVICE_PORT"), chi.URLParam(r, "user_id")))
	if err != nil {
		log.Println(err)
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(fmt.Sprintf("Failed: %s", err.Error())))
		return
	}
	defer responseUser.Body.Close()

	if responseUser.StatusCode == http.StatusNoContent {
		w.WriteHeader(http.StatusNoContent)
		return
	}

	var tasksUser []model.Item
	for _, task := range database.Items {
		if task.UserId == userId {
			tasksUser = append(tasksUser, task)
		}
	}

	if len(tasksUser) == 0 {
		w.WriteHeader(http.StatusOK)
		w.Write([]byte("User without tasks"))
	}

	response, err := json.Marshal(tasksUser)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(fmt.Sprintf("Failed: %s", err.Error())))
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Write(response)
}

func (t *toDoController) GetTaskInfo(w http.ResponseWriter, r *http.Request) {
	taskId, err := strconv.Atoi(chi.URLParam(r, "task_id"))
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(fmt.Sprintf("Failed: %s", err.Error())))
		return
	}

	for _, task := range database.Items {
		if task.Id == taskId {
			response, err := json.Marshal(task)
			if err != nil {
				w.WriteHeader(http.StatusInternalServerError)
				w.Write([]byte(fmt.Sprintf("Failed: %s", err.Error())))
				return
			}

			w.WriteHeader(http.StatusOK)
			w.Write(response)
			return
		}
	}

	w.WriteHeader(http.StatusNoContent)
	w.Write([]byte("User not found"))
}
