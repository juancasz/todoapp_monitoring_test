package main

import (
	"log"
	"net/http"
	"os"
	"todo_service/controllers"
	"todo_service/router"
)

func main() {
	port := os.Getenv("PORT")

	controller := controllers.Controller{
		ToDoController: controllers.NewToDoController(),
	}

	r := router.NewRouter(controller)

	log.Printf(`http server is running on port "%s"`, port)
	http.ListenAndServe(":"+port, r)
}
