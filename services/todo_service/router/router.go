package router

import (
	"todo_service/controllers"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/cors"
)

func NewRouter(controller controllers.Controller) *chi.Mux {
	r := chi.NewRouter()

	r.Use(cors.Handler(cors.Options{
		AllowedOrigins: []string{"http://*"},
	}))

	r.Get("/", controller.ToDoController.CheckService)
	r.Get("/lists/users/{user_id}", controller.ToDoController.GetUsersTasks)
	r.Get("/lists/{task_id}", controller.ToDoController.GetTaskInfo)

	return r
}
