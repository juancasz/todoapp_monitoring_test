package model

type (
	Item struct {
		Id     int    `json:"id,omitempty"`
		UserId int    `json:"user_id,omitempty"`
		Task   string `json:"task,omitempty"`
	}
	User struct {
		Id   int    `json:"id,omitempty"`
		Name string `json:"name,omitempty"`
	}
)
