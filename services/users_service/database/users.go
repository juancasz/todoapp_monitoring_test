package database

import "users_service/model"

var Users = []model.User{
	{Id: 1, Name: "Juan"},
	{Id: 2, Name: "Laura"},
	{Id: 3, Name: "Javier"},
}
