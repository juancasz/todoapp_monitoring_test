package controllers

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"strconv"
	"users_service/database"

	"github.com/go-chi/chi/v5"
)

type UserController interface {
	CheckService(http.ResponseWriter, *http.Request)
	GetUsers(http.ResponseWriter, *http.Request)
	GetUserById(http.ResponseWriter, *http.Request)
	GetUserTasks(http.ResponseWriter, *http.Request)
}

func NewUsersController() UserController {
	return &usersController{}
}

type usersController struct {
}

func (u *usersController) CheckService(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)
	w.Write([]byte("User service is up"))
}

func (u *usersController) GetUsers(w http.ResponseWriter, r *http.Request) {
	users := database.Users

	response, err := json.Marshal(users)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(fmt.Sprintf("Failed: %s", err.Error())))
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Write(response)
}

func (u *usersController) GetUserById(w http.ResponseWriter, r *http.Request) {
	userId, err := strconv.Atoi(chi.URLParam(r, "id"))
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(fmt.Sprintf("Failed: %s", err.Error())))
		return
	}

	for _, user := range database.Users {
		if user.Id == userId {
			response, err := json.Marshal(user)
			if err != nil {
				w.WriteHeader(http.StatusInternalServerError)
				w.Write([]byte(fmt.Sprintf("Failed: %s", err.Error())))
				return
			}

			w.WriteHeader(http.StatusOK)
			w.Write(response)
			return
		}
	}

	w.WriteHeader(http.StatusNoContent)
	w.Write([]byte("User not found"))
}

func (u *usersController) GetUserTasks(w http.ResponseWriter, r *http.Request) {
	_, err := strconv.Atoi(chi.URLParam(r, "id"))
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(fmt.Sprintf("Failed: %s", err.Error())))
		return
	}

	responseList, err := http.Get(fmt.Sprintf("http://todo-api:%s/lists/users/%s", os.Getenv("LISTS_SERVICE_PORT"), chi.URLParam(r, "id")))
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(fmt.Sprintf("Failed: %s", err.Error())))
		return
	}
	defer responseList.Body.Close()

	if responseList.StatusCode == http.StatusNoContent {
		w.WriteHeader(http.StatusNoContent)
		return
	}

	response, err := ioutil.ReadAll(responseList.Body)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(fmt.Sprintf("Failed: %s", err.Error())))
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Write(response)
}
