package router

import (
	"users_service/controllers"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/cors"
)

func NewRouter(controller controllers.Controller) *chi.Mux {
	r := chi.NewRouter()

	r.Use(cors.Handler(cors.Options{
		AllowedOrigins: []string{"http://*"},
	}))

	//routes
	r.Get("/", controller.UserController.CheckService)
	r.Get("/users", controller.UserController.GetUsers)
	r.Get("/users/{id}", controller.UserController.GetUserById)
	r.Get("/users/{id}/lists", controller.UserController.GetUserTasks)

	return r
}
