package main

import (
	"log"
	"net/http"
	"os"
	"users_service/controllers"
	"users_service/router"
)

func main() {
	port := os.Getenv("PORT")

	controller := controllers.Controller{
		UserController: controllers.NewUsersController(),
	}

	r := router.NewRouter(controller)

	log.Printf(`http server is running on port "%s"`, port)
	http.ListenAndServe(":"+port, r)
}
